'use strict';

/**
 * @package mongoose-paginate
 * @param {Object} [query={}]
 * @param {Object} [options={}]
 * @param {Object|String} [options.select]
 * @param {Object|String} [options.sort]
 * @param {Array|Object|String} [options.populate]
 * @param {Boolean} [options.lean=false]
 * @param {Boolean} [options.leanWithId=true]
 * @param {Number} [options.offset=0] - Use offset or page to set skip position
 * @param {Number} [options.page=1]
 * @param {Number} [options.limit=10]
 * @param {Function} [callback]
 * @returns {Promise}
 */

async function paginate(query, options, callback) {
    query = query || {};
    options = Object.assign({}, paginate.options, options);
    const select = options.select;
    const sort = options.sort;
    const populate = options.populate;
    const lean = options.lean || false;
    const leanWithId = options.leanWithId ? options.leanWithId : true;

    const limit =
        options.limit !== null && options.limit !== undefined
            ? options.limit
            : 10;

    let page, offset, skip, promises;
    if (options.offset) {
        offset = options.offset;
        skip = offset;
    } else if (options.page) {
        page = options.page;
        skip = (page - 1) * limit;
    } else {
        page = 1;
        offset = 0;
        skip = offset;
    }

    let docsQuery = [];

    if (limit) {
        docsQuery = this.find(query)
            .select(select)
            .sort(sort)
            .skip(skip)
            .limit(limit)
            .lean(lean);
        if (populate) {
            [].concat(populate).forEach(item => {
                docsQuery.populate(item);
            });
        }
    }

    promises = {
        docs: docsQuery,
        count: this.countDocuments(query)
    };
    if (lean && leanWithId) {
        promises.docs = promises.docs.then(docs => {
            docs.forEach(doc => {
                doc.id = String(doc._id);
            });
            return docs;
        });
    }

    promises = Object.keys(promises).map(x => promises[x]);

    return Promise.all(promises).then(([docs, total]) => {
        const result = {
            docs,
            total,
            limit
        };
        if (offset !== undefined) {
            result.offset = offset;
        }
        if (page !== undefined) {
            result.page = page;
            result.pages = Math.ceil(total / limit) || 1;
        }
        if (typeof callback === 'function') {
            return callback(null, result);
        }
        return new Promise(resolve => resolve(result));
    });
}

/**
 * @param {Schema} schema
 */

module.exports = function(schema) {
    schema.statics.paginate = paginate;
};

module.exports.paginate = paginate;
